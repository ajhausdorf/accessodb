Rails.application.routes.draw do

  get 'shop_sessions/new'

  get 'shops/new'

  get 'cars/new'

  get 'sessions/new'

  get 'users/new'

  root             'static_pages#home'
  get 'shophome' => 'static_pages#shop_home', as:"shop_home"
  get 'shop', to: 'static_pages#shop_home', as: 'shop_root'
  get 'shops', to: 'shops#index', as: 'shop_index'
  get 'help'    => 'static_pages#help'
  get 'about'   => 'static_pages#about'
  get 'contact' => 'static_pages#contact'
  get 'signup'  => 'users#new'
  get 'shopsignup' => 'shops#new'
  get    'login'   => 'sessions#new'
  post   'login'   => 'sessions#create'
  get    'shoplogin'   => 'shop_sessions#new', as: 'shop_login'
  post   'shoplogin'   => 'shop_sessions#create'
  delete 'logout'  => 'sessions#destroy'
  delete 'shoplogout'  => 'shop_sessions#destroy', as: 'shop_logout'
  post 'updateshop' => 'users#update_shop', as: 'update_shop'
  get 'customers' => 'shops#customers'
  resources :shops do 
    get 'customers', on: :member 
  end
  
  resources :users
  resources :cars,  only: [:create, :destroy, :edit]
end
