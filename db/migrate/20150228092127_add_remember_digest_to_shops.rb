class AddRememberDigestToShops < ActiveRecord::Migration
  def change
    add_column :shops, :remember_digest, :string
  end
end
