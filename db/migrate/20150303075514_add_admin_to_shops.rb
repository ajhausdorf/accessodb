class AddAdminToShops < ActiveRecord::Migration
  def change
    add_column :shops, :admin, :boolean, default: false
  end
end
