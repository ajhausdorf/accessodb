class AddIndexToShopsEmail < ActiveRecord::Migration
  def change
    add_index :shops, :email, unique: true
  end
end
