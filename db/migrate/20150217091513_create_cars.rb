class CreateCars < ActiveRecord::Migration
  def change
    create_table :cars do |t|
      t.text :year
      t.text :brand
      t.text :model
      t.text :vin
      t.text :mileage
      t.references :user, index: true

      t.timestamps null: false
    end
    add_foreign_key :cars, :users
  end
end
