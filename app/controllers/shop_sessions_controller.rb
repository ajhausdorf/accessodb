class ShopSessionsController < ApplicationController

  def new
  end
  
  def create
    shop = Shop.find_by(email: params[:session][:email].downcase)
    if shop && shop.authenticate(params[:session][:password])
      shop_log_in shop
      params[:session][:remember_me] == '1' ? shop_remember(shop) : shop_forget(shop)
      redirect_to shop_home_path
    else
      flash.now[:danger] = 'Invalid email/password combination'
      render 'new'
    end
  end

  def destroy
    shop_logout if shop_logged_in?
    redirect_to root_url
  end
end
