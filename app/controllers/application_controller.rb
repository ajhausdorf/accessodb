class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  include SessionsHelper
  include ShopSessionsHelper
  
  private
  
    def logged_in_user
      unless logged_in?
        store_location
        flash[:danger] = "Please log in."
        redirect_to login_url
      end
    end
    
      def logged_in_shop
      unless shop_logged_in?
        store_location
        flash[:danger] = "Please log in."
        redirect_to shop_login_url
      end
    end
end
