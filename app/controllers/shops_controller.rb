class ShopsController < ApplicationController
  before_action :logged_in_shop, only: [:edit, :update]
  before_action :logged_in_user, only: :destroy
  before_action :correct_shop, only: [:edit, :update]
  before_action :admin_user,     only: :destroy
     
    def index
      @shops = Shop.all
    end
    
    def show
      @shop = Shop.find(params[:id])
      @user = current_user
    end
    
    def new
      @shop = Shop.new
    end
    
    def create
      @shop = Shop.new(shop_params)
      if @shop.save
        shop_log_in @shop
        flash[:success] = "Thank you for signing up, welcome to Ensage!"
        redirect_to shop_home_path
      else
        render 'new'
      end
    end
    
    def edit
      @shop = Shop.find(params[:id])
    end

    def update
      @shop = Shop.find(params[:id])
      if @shop.update_attributes(shop_params)
        flash[:success] = "Profile Updated"
        redirect_to @shop
      else
          render 'edit'
      end
    end
    
    def destroy
      Shop.find(params[:id]).destroy
      flash[:success] = "Shop Deleted"
      redirect_to shops_url
    end

    def customers
      @shop = current_shop
      @customers = @shop.users
    end
    
    private

    def shop_params
      params.require(:shop).permit(:name, :address, :city, :state, :zip, :email, :phone, :password,
                                   :password_confirmation, :picture)
    end
    
    def correct_shop
      @shop = Shop.find(params[:id])
      redirect_to(root_url) unless current_shop?(@shop)
    end
    
    def admin_user
      redirect_to(root_url) unless current_user.admin?
    end

end