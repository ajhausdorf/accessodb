class CarsController < ApplicationController
  before_action :logged_in_user, only: [:create, :destroy, :edit]
  before_action :correct_user, only: [:destroy, :edit]
  
  def create
      @car = current_user.cars.build(cars_params)
      respond_to do |format|
          if @car.save
              @feed_items = current_user.feed
              format.html { redirect_to root_url }
              format.js
          else
              format.html { render 'static_pages/home' }
              format.js
          end
      end
  end
  
  def destroy
    @car.destroy
    flash[:success] = "Car deleted"
    redirect_to request.referrer || root_url
  end
    
  def edit
    @car.edit
    flash[:success] = "Car edited"
    redirect_to request.referrer || root_url
  end
  
  private
  
    def cars_params
      params.require(:car).permit(:year, :brand, :model, :vin, :mileage, :picture)
    end
    
    def correct_user
      @car = current_user.cars.find_by(id: params[:id])
      redirect_to root_url if @car.nil?
    end
end
