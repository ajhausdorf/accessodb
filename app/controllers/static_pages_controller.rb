class StaticPagesController < ApplicationController

  def home
    if logged_in?
      @car = current_user.cars.build
      @feed_items = current_user.feed.paginate(page: params[:page])
    end
  end
  
  def shop_home
    if shop_logged_in?
      @shop = current_shop
    end
  end

  def help
  end
  
  def about 
  end
  
  def contact
  end
end
