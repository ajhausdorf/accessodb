module ShopSessionsHelper

  # Logs in the given shop.
  def shop_log_in(shop)
    session[:shop_id] = shop.id
  end

  # Remembers a shop in a persistent session.
  def shop_remember(shop)
    shop.remember
    cookies.permanent.signed[:shop_id] = shop.id
    cookies.permanent[:remember_token] = shop.remember_token
  end
  
  def current_shop?(shop)
    shop == current_shop
  end

  # Returns the shop corresponding to the remember token cookie.
  def current_shop
    if (shop_id = session[:shop_id])
      @current_shop ||= Shop.find_by(id: shop_id)
    elsif (shop_id = cookies.signed[:shop_id])
      shop = Shop.find_by(id: shop_id)
      if shop && shop.authenticated?(cookies[:remember_token])
        shop_log_in shop
        @current_shop = shop
      end
    end
  end

  # Returns true if the shop is logged in, false otherwise.
  def shop_logged_in?
    !current_shop.nil?
  end
  
  def shop_forget(shop)
    shop.forget
    cookies.delete(:shop_id)
    cookies.delete(:remember_token)
  end

  # Logs out the current shop.
  def shop_logout
    shop_forget(current_shop)
    session.delete(:shop_id)
    @current_shop = nil
  end

  # Redirects to stored location (or to the default).
  def shop_redirect_back_or(default)
    redirect_to(shop)
    session.delete(:forwarding_url)
  end

  # Stores the URL trying to be accessed.
  def store_location
    session[:forwarding_url] = request.url if request.get?
  end
end