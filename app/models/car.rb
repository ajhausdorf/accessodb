class Car < ActiveRecord::Base
  belongs_to :user
  mount_uploader :picture, PictureUploader
  validates :user_id, presence: true
  validates :year, presence:true, length: { maximum: 4 }
  validates :brand, presence:true
  validates :model, presence:true
  validates :vin, presence:true, length: { maximum: 17 }
  validates :mileage, presence:true
  validate  :picture_size
  
  private
  
    def picture_size
      if picture.size > 5.megabytes
        errors.add(:picture, "should be less than 5MB")
      end
    end
end
