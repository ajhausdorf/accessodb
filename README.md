# Ruby on Rails application for AccessOBD 

AccessOBD provides automotive garages with access to near real time vehicle 
data. After a Bluetooth OBDII reader has been installed in the port of a 
client's vehicle, an app on the owner's phone will capture information such 
as mileage and Diagnostic Trouble Codes. 

This information will be displayed for the owner and provided to the shop 
through an online portal. The garage will be empowered to provide timely 
reminders of scheduled maintenance, as well as notifications when an 
engine malfunctions.

Our market research has indicated that a garage's biggest need is to track mileage to keep car owners on their maintenance schedule, so our MVP will begin here.  An android app will read mileage from the OBDII dongle and allow mechanics to send reminders to their customers when maintenance is needed.